# Assumptions and motivations

We want to build a system which is maximally flexible and efficient, but also consistent at the same time. The following main processes should be considered:

1. Receive the data
2. Retrieve and update player's details
3. Maintain the pool of waiting players
4. Match the players according to the requirements
5. Deliver the matched players

The idea is to make each of the mentioned processes as independent of each other as possible, so that none has to wait for the outcome of the other. We are getting close to this goal by using separate actors for each main process. The only limitation though is dictated by the need of keeping the consistency: we can only match one player at a time. It is impossible to run several matches in parallel because after each match the queue of waiting players will be modified accordingly (the matched players will be removed), and the next match should be made based on the updated queue.

However, apart from the limitation mentioned above, all other processes can run in an almost parallel mode: the retrieval and update of the newly arrived player details will not affect the waiting pool management, and the pool management will not affect the matching computations.

# Implementation

In order to achieve the goals defined above I chose the Akka (typed) toolkit for actor systems development, which allows for non-blocking messaging between components, providing convenient control of multi-threading, parallelism and concurrency.
The implemented service could be easily integrated both into a distributed or a monolithic system.

The main components and their functionalities are:

* PlayerProvider: Knows how to operate on the `Player` data. It doesn't have direct access to the persistence (where the `Player` data is stored), hence will talk to a `PlayerRepo`implementation in order to retrieve, update and store `Player`s. After retrieving the `Player` by id and updating it, the `PlayerProvider` sends a`NewWaitingPlayer(player)` message to the `WaitingPool`.
* PlayerRepo: A layer of abstraction between the business logic and the persistence layer. A simple implementation based on `Map` is provided.
* WaitingPool: Stores the `Player`s (wrapped as `WaitingPlayer`s) that need to be matched, sending for them match requests to the `Matcher`:
   * in the order of arrival time
   * when the wait time is over
   * when the settings changes might require it (e.g. increased max score delta)
* Matcher: Computes the matches for the `WaitingPlayer`s indicated in the requests from the `WaitingPool`.
* Output: Receives the matched players ans should be in charge to send the results further according to the requirements and the chosen technology. Currently, it's a simplistic implementation just logging the received results.

Please note that the current implementation has much logging at INFO level, intended for demo purposes.

# How it works

The system can receive three types of input:

1. A new player willing to enter the game, i.e. joining the `WaitingPool`: `Long`, as player's ID.
1. A game outcome: (`Long`, `Long`), as winner's and loser's IDs.
1. New settings to be applied to matching: (`Double`, `Long`), as `maxScoreDelta` and `maxWaitMs`.

The output is: (`Long`, `Long`), as player IDs.

Both input and output data can be adjusted to be ingested and delivered in different formats, according to requirements.

The detailed specification of each component can be checked in the tests.
In order to try how the service works, you can run the provided sample `MatchingServiceApp`. This will produce a verbose logging output that will let you observe all the events inside the system. Feel free to add players and types of input messages.

# What is not implemented

* The recovery strategy for the actor/s that could fail and/or be terminated by an exception
* The re-try strategy for the cases when the response failed
* Back pressure and congestion management