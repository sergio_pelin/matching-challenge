package pixelcrack.speed.matching.actors.helpers

import akka.actor.typed.Behavior
import org.scalactic.{Bad, ErrorMessage, Good, Or}
import pixelcrack.speed.matching.actors.PlayerRepo
import pixelcrack.speed.model.{Player, PlayerId}

object TestPlayerRepo {
  private val errorPlayerId = PlayerId(666)
  val updateErrorMessage = "BOOM!!!"
  def updateErrorPlayer(player: Player): Player = player.copy(id = errorPlayerId)

  def havingValidOnly(validPlayers: Player*): Behavior[PlayerRepo.Query] = {
    new TestPlayerRepo(validPlayers).repo()
  }

  def apply(validPlayers: Seq[Player]): Behavior[PlayerRepo.Query] = havingValidOnly(validPlayers:_*)
}

class TestPlayerRepo private (validPlayers: Seq[Player]) extends PlayerRepo {
  import TestPlayerRepo._

  override protected def findById(playerId: PlayerId): Or[Player, ErrorMessage] =
    validPlayers.find(_.id == playerId) match {
      case Some(player) => Good(player)
      case _ => Bad(s"Player $playerId not found")
    }

  override protected def update(playerId: PlayerId, player: Player): Or[Unit, ErrorMessage] = {
    if (playerId == errorPlayerId) Bad(updateErrorMessage)
    else Good(player)
  }
}