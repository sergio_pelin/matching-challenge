package pixelcrack.speed.matching.actors.helpers

import akka.actor.testkit.typed.scaladsl.{FishingOutcomes, ScalaTestWithActorTestKit, TestProbe}
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import pixelcrack.speed.matching.actors.Matcher.MatcherCommand
import pixelcrack.speed.matching.actors.Output.OutputCommand
import pixelcrack.speed.matching.actors.WaitingPlayerTimer
import pixelcrack.speed.matching.actors.WaitingPlayerTimer.PlayerTimerCommand
import pixelcrack.speed.matching.actors.WaitingPool.WaitingPoolCommand
import pixelcrack.speed.matching.{WaitKey, WaitingPlayer}
import pixelcrack.speed.model.{Player, PlayerId, Settings}

import scala.concurrent.duration.{DurationInt, DurationLong, FiniteDuration}

object ActorSpecHelper {
  val defaultSettings: Settings = Settings(maxScoreDelta = 0.01, maxWaitMs = 1000)
}

trait ActorSpecHelper { self: ScalaTestWithActorTestKit =>

  val defaultSettings: Settings = ActorSpecHelper.defaultSettings
  val noTimerEffectSettings: Settings = defaultSettings.copy(maxWaitMs = defaultSettings.maxWaitMs * 600)
  val immediately: FiniteDuration = 1.millis
  val durationFishing: FiniteDuration = 3.seconds

  val player1: Player = Player(PlayerId(1), score = 0.5, playedOpponents = Set.empty)
  val player2: Player = Player(PlayerId(2), score = 0.75, playedOpponents = Set.empty)
  val player3: Player = Player(PlayerId(3), score = 1.0, playedOpponents = Set.empty)
  val player4: Player = Player(PlayerId(4), score = 1.25, playedOpponents = Set.empty)
  val player5: Player = Player(PlayerId(5), score = 1.5, playedOpponents = Set.empty)
  val blockingPlayer: Player = player2.copy(id = PlayerId(MonitoredMatcher.blockingPlayerId))


  def wPlayerFor(
  player: Player,
  wPool: ActorRef[WaitingPoolCommand],
  maxWaitMs: Long = defaultSettings.maxWaitMs,
  delay: Long = 0): WaitingPlayer = {
    val arrivalTimeMs: Long = System.currentTimeMillis() + delay
    val waitKey = WaitKey(arrivalTimeMs, player)
    val timer: ActorRef[PlayerTimerCommand] = testKit.spawn(WaitingPlayerTimer(waitKey, arrivalTimeMs, maxWaitMs, wPool))
    WaitingPlayer(player, waitKey, timer)
  }

  def waitKeyFor(player: Player): WaitKey = WaitKey(System.currentTimeMillis(), player)

  def mockWaitingPool(probe: TestProbe[WaitingPoolCommand], mockBehavior: Behavior[WaitingPoolCommand]): ActorRef[WaitingPoolCommand] =
    testKit.spawn[WaitingPoolCommand](Behaviors.monitor(probe.ref, mockBehavior))

  def monitoredWaitingPool(
    wPoolProbe: TestProbe[WaitingPoolCommand],
    matcherProbe: TestProbe[MatcherCommand],
    outputProbe: TestProbe[OutputCommand] = testKit.createTestProbe[OutputCommand](),
    settings: Settings = defaultSettings): ActorRef[WaitingPoolCommand] =
    testKit.spawn(MonitoredWaitingPool(wPoolProbe.ref, matcherProbe.ref, outputProbe.ref, settings = settings))

  def watch(probe: TestProbe[WaitingPoolCommand], timers: Seq[ActorRef[PlayerTimerCommand]]): ActorRef[WaitingPoolCommand] = {
    testKit.spawn[WaitingPoolCommand](
      Behaviors.monitor(probe.ref,
        Behaviors.setup { context =>
          timers.foreach(context.watch)
          Behaviors.same
        })
    )
  }

  /**
   * Just a pause to make the processes take place before checking things
   */
  def waitForInternalEffectsMs(ms: Long): Unit = Thread.sleep(ms)

  implicit class TestPlayerOps(player: Player) {

    def adjustScoreDeltaAgainst(players: Seq[Player], diff: Double): Player = {
      val maxScore = players.map(_.score).max
      player.copy(score = maxScore + diff)
    }

    def makeNonMatchingTo(players: Player*): Player = adjustScoreDeltaAgainst(players, diff = defaultSettings.maxScoreDelta * 2)

    def makeMatchingTo(players: Player*): Player =
      adjustScoreDeltaAgainst(players, diff = defaultSettings.maxScoreDelta - defaultSettings.maxScoreDelta / 10)
  }

  def playerTimerWith(
    waitMs: Long,
    waitingPoolProbe: ActorRef[WaitingPoolCommand],
    arrivalTimeMs: Long = System.currentTimeMillis(),
    waitKeyOpt: Option[WaitKey] = None): ActorRef[WaitingPlayerTimer.PlayerTimerCommand] = {
    val waitKey: WaitKey = waitKeyOpt.getOrElse(WaitKey(arrivalTimeMs, player1))
    testKit.spawn(WaitingPlayerTimer(waitKey, arrivalTimeMs, waitMs, waitingPoolProbe))
  }

  def aBitLongerThan(ms: Long, fractionToAdd: Int = 10): FiniteDuration = (ms + ms / fractionToAdd).millis

  def playerWithScoreDiff(player: Player, diff: Double, f: (Double, Double) => Double): Player =
    player.copy(score = f(player.score, diff))

  def fishForMessage[M](probe: TestProbe[M], expectedMsg: M, repeated: Int = 1,  duration: FiniteDuration = durationFishing): Seq[M] = {
    var pending = repeated
    probe.fishForMessage(duration) { actualMsg =>
      if (actualMsg == expectedMsg) {
        pending -= 1
        if (pending == 0) FishingOutcomes.complete
        else FishingOutcomes.continue
      }
      else FishingOutcomes.continue
    }
  }
}
