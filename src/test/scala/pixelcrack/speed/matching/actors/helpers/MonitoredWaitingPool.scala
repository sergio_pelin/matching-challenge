package pixelcrack.speed.matching.actors.helpers

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import pixelcrack.speed.matching.actors.Matcher.MatcherCommand
import pixelcrack.speed.matching.actors.Output.OutputCommand
import pixelcrack.speed.matching.actors.WaitingPool
import pixelcrack.speed.matching.actors.WaitingPool.WaitingPoolCommand
import pixelcrack.speed.matching.actors.helpers.ActorSpecHelper._
import pixelcrack.speed.matching.{WaitKey, WaitingPlayer}
import pixelcrack.speed.model.Settings

import scala.collection.immutable.SortedMap

object MonitoredWaitingPool {

  def apply(
    waitingPoolProbe: ActorRef[WaitingPoolCommand],
    matcherProbe: ActorRef[MatcherCommand],
    outputProbe: ActorRef[OutputCommand],
    settings: Settings = defaultSettings,
    waitingPlayers: SortedMap[WaitKey, WaitingPlayer] = SortedMap.empty,
    blockingMatcherMs: Int = 0): Behavior[WaitingPoolCommand] = {

    Behaviors.monitor(waitingPoolProbe,
      Behaviors.setup { context =>
        val matcher =
          context.spawn(
            MonitoredMatcher(matcherProbe, context.self, blockingDurationMs = blockingMatcherMs),
            "monitored-matcher")
        new MonitoredWaitingPool(context, settings, waitingPlayers, matcher, outputProbe.ref).running
      }
    )
  }
}

class MonitoredWaitingPool private(
  context: ActorContext[WaitingPoolCommand],
  settings: Settings,
  waitingPlayers: SortedMap[WaitKey, WaitingPlayer],
  matcher: ActorRef[MatcherCommand],
  outputManager: ActorRef[OutputCommand]) extends WaitingPool(context, settings, waitingPlayers, matcher, outputManager)