package pixelcrack.speed.matching.actors.helpers

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, BehaviorInterceptor, TypedActorContext}
import pixelcrack.speed.matching.actors.Matcher
import pixelcrack.speed.matching.actors.Matcher.{FindMatchRequest, MatcherCommand}
import pixelcrack.speed.matching.actors.WaitingPool.WaitingPoolCommand
import pixelcrack.speed.matching.actors.helpers.ActorSpecHelper.defaultSettings

object MonitoredMatcher {

  /**
   * A convenience marker in order to block the Matcher,
   * while testing the state resulted from previous commands effects.
   */
  val blockingPlayerId: Int = 666666

  /**
   * @param probe Used with `Behaviors.monitor` will receive all the commands reaching the `Matcher`
   * @param waitingPool The `WaitingPool` to which the `Matcher` reports
   * @param blockingDurationMs Duration in milliseconds when blocking is used to create a pause in `Matcher`'s work
   * @return Standard `Matcher` behaviour
   */
  def apply(
    probe: ActorRef[MatcherCommand],
    waitingPool: ActorRef[WaitingPoolCommand],
    blockingDurationMs: Long = 0): Behavior[MatcherCommand] =
    Behaviors.monitor(probe,
      Behaviors.intercept(
        () => new BehaviorInterceptor[MatcherCommand, MatcherCommand]() {
          override def aroundReceive(
            ctx: TypedActorContext[MatcherCommand],
            msg: MatcherCommand,
            target: BehaviorInterceptor.ReceiveTarget[MatcherCommand]): Behavior[MatcherCommand] =
            msg match {
              case FindMatchRequest(wPlayer) if wPlayer.player.id.value == blockingPlayerId =>
                Thread.sleep(blockingDurationMs)
                target(ctx, msg)
              case _ =>
                target(ctx, msg)
            }
        }
      )
      (Matcher(waitingPool, defaultSettings.maxWaitMs))
    )
}
