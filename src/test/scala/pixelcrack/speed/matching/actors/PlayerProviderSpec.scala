package pixelcrack.speed.matching.actors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike
import pixelcrack.speed.matching.actors.PlayerProvider.{NewGameResult, NewPlayer, updatedLoser, updatedWinner}
import pixelcrack.speed.matching.actors.WaitingPool.{NewWaitingPlayer, WaitingPoolCommand}
import pixelcrack.speed.matching.actors.helpers.{ActorSpecHelper, TestPlayerRepo}

class PlayerProviderSpec extends ScalaTestWithActorTestKit with ActorSpecHelper with AnyWordSpecLike {

  "When receiving an existing playerId" should {
    "retrieve the Player from the PlayerRepo and send it to WaitingPool as a WaitingPlayer" in {
      val validPlayer = player1
      val waitingPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val playerRepo = testKit.spawn(TestPlayerRepo.havingValidOnly(validPlayer))

      val ppUnderTest = testKit.spawn(PlayerProvider(playerRepo, waitingPoolProbe.ref))

      ppUnderTest ! NewPlayer(validPlayer.id)

      waitingPoolProbe.expectMessage(NewWaitingPlayer(validPlayer))
    }
  }

  "When receiving a invalid playerId" should {
    "do nothing" in { //TODO Could be replaced with sending warning notification
      val validPlayer = player1
      val invalidPlayer = player2
      val waitingPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val playerRepo = testKit.spawn(TestPlayerRepo.havingValidOnly(validPlayer))

      val ppUnderTest = testKit.spawn(PlayerProvider(playerRepo, waitingPoolProbe.ref))

      ppUnderTest ! NewPlayer(invalidPlayer.id)

      waitingPoolProbe.expectNoMessage
    }
  }

  "When receiving a Game result with valid players" should {
    "send the players with updated scores to the WaitingPool as WaitingPlayers" in {
      val waitingPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()

      val winner = player1
      val loser = player2
      val playerRepo = testKit.spawn(TestPlayerRepo.havingValidOnly(winner, loser))

      val ppUnderTest = testKit.spawn(PlayerProvider(playerRepo, waitingPoolProbe.ref))

      ppUnderTest ! NewGameResult(winner.id, loser.id)

      val collectedMessages: Seq[WaitingPoolCommand] = Seq(
        updatedWinner(winner, loser),
        updatedLoser(loser, winner))
        .map(NewWaitingPlayer)
        .flatMap(msg => fishForMessage(waitingPoolProbe, msg))

      collectedMessages should have size 2
    }
  }


  "When receiving a Game result with one invalid player" should {
    "do nothing" in { //TODO Could be replaced with sending warning notification
      val waitingPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()

      val winner = player1
      val loser = player2

      val playerRepo = testKit.spawn(TestPlayerRepo.havingValidOnly(winner))

      val ppUnderTest = testKit.spawn(PlayerProvider(playerRepo, waitingPoolProbe.ref))

      ppUnderTest ! NewGameResult(winner.id, loser.id)

      waitingPoolProbe.expectNoMessage
    }
  }

  //TODO Add testing for:
  // 1. failures/exceptions as result of a request, proving resilience to downstream errors
  // 2. the application of retry strategy

}
