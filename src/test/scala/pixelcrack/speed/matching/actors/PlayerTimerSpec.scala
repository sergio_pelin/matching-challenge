package pixelcrack.speed.matching.actors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike
import pixelcrack.speed.matching.WaitKey
import pixelcrack.speed.matching.actors.WaitingPlayerTimer.UpdateTimer
import pixelcrack.speed.matching.actors.WaitingPool.{TimeIsUpFor, WaitingPoolCommand}
import pixelcrack.speed.matching.actors.helpers.ActorSpecHelper

import scala.concurrent.duration.DurationInt

class PlayerTimerSpec extends ScalaTestWithActorTestKit with AnyWordSpecLike with ActorSpecHelper  {

  "When created" should {
    "send a TimeIsUpFor(waitKey) after the `maxWaitMs` used as parameter" in {
      val waitMs = defaultSettings.maxWaitMs
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val arrivalTimeMs = System.currentTimeMillis()
      val waitKey = WaitKey(arrivalTimeMs, player1)

      playerTimerWith(waitMs, wPoolProbe.ref, arrivalTimeMs, Some(waitKey))

      wPoolProbe.expectMessage(aBitLongerThan(waitMs), TimeIsUpFor(waitKey))
    }
  }

  "When being updated with longer `waitMs`" should {
    "send a TimeIsUpFor(waitKey) after the updated `maxWaitMs`" in {
      val waitMs = defaultSettings.maxWaitMs
      val longerWaitMs = waitMs * 3
      val initialWaitMs = waitMs / 2
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val arrivalTimeMs = System.currentTimeMillis()
      val waitKey = WaitKey(arrivalTimeMs, player1)

      val timerUnderTest = playerTimerWith(waitMs, wPoolProbe.ref, arrivalTimeMs, Some(waitKey))

      waitForInternalEffectsMs(initialWaitMs)

      timerUnderTest ! UpdateTimer(longerWaitMs)

      wPoolProbe.expectNoMessage((longerWaitMs - initialWaitMs).millis)

      wPoolProbe.expectMessage(TimeIsUpFor(waitKey))
    }
  }

  "When being updated with shorter `waitMs`" should {
    "send a TimeIsUpFor(waitKey) after the updated `maxWaitMs`" in {
      val waitMs = defaultSettings.maxWaitMs * 2
      val shorterWaitMs = waitMs / 2
      val initialWaitMs = shorterWaitMs / 4
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val arrivalTimeMs = System.currentTimeMillis()
      val waitKey = WaitKey(arrivalTimeMs, player1)

      val timerUnderTest = playerTimerWith(waitMs, wPoolProbe.ref, arrivalTimeMs, Some(waitKey))

      waitForInternalEffectsMs(initialWaitMs)

      timerUnderTest ! UpdateTimer(shorterWaitMs)

      wPoolProbe.expectMessage(shorterWaitMs.millis, TimeIsUpFor(waitKey))
    }
  }

  "When being updated with shorter `waitMs` which makes the current wait complete" should {
    "send a TimeIsUpFor(waitKey) immediately" in {
      val waitMs = defaultSettings.maxWaitMs
      val initialWaitMs = waitMs / 2
      val shorterWaitMs = waitMs / 4
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val arrivalTimeMs = System.currentTimeMillis()
      val waitKey = WaitKey(arrivalTimeMs, player1)

      val timerUnderTest = playerTimerWith(waitMs, wPoolProbe.ref, arrivalTimeMs, Some(waitKey))

      waitForInternalEffectsMs(initialWaitMs)

      timerUnderTest ! UpdateTimer(shorterWaitMs)

      wPoolProbe.expectMessage(immediately, TimeIsUpFor(waitKey))
    }
  }
}
