package pixelcrack.speed.matching.actors

import akka.actor.testkit.typed.scaladsl.{FishingOutcomes, LogCapturing, ScalaTestWithActorTestKit, TestProbe}
import org.scalatest.wordspec.AnyWordSpecLike
import pixelcrack.speed.matching.WaitingPlayer
import pixelcrack.speed.matching.actors.Matcher._
import pixelcrack.speed.matching.actors.Output.{Matched, OutputCommand}
import pixelcrack.speed.matching.actors.WaitingPool._
import pixelcrack.speed.matching.actors.helpers.ActorSpecHelper

import scala.concurrent.duration.DurationDouble

class WaitingPoolSpec extends ScalaTestWithActorTestKit with ActorSpecHelper with AnyWordSpecLike with LogCapturing {

  "On NewWaitingPlayer(player)" should {
    "add the player to waitingPlayers queue and send FindMatchRequest(wPlayer) to the Matcher" in {
      val testSettings = noTimerEffectSettings
      val testPlayer = player1
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe, settings = testSettings)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(wPlayers, _) if wPlayers.isEmpty => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolUnderTest ! NewWaitingPlayer(testPlayer)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(wPlayers, _) if wPlayers.nonEmpty && wPlayers.head.player == testPlayer => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case FindMatchRequest(wPlayer) if wPlayer.player == testPlayer => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "On TimeIsUpFor(key)" should {
    "mark the player as overtime and send FindMatchRequest(wPlayer) to the Matcher" in {
      val testPlayer = player1
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe)

      wPoolUnderTest ! NewWaitingPlayer(testPlayer)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(wPlayers, _) if !wPlayers.head.isOvertime => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      waitForInternalEffectsMs(defaultSettings.maxWaitMs / 2)
      wPoolProbe.fishForMessage(durationFishing){
        case TimeIsUpFor(_) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(wPlayers, _) if wPlayers.head.isOvertime => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case FindMatchRequest(wPlayer) if wPlayer.player == testPlayer => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "On UpdateSettings, if both maxScoreDelta and maxWaitMs are changed, " +
    "where (new maxScoreDelta) < (old maxScoreDelta) and (new maxWaitMs) < (old maxWaitMs)" should {
    "update the settings for matching and update the timers of `WaitingPlayer`s" in {
      val initialSettings = defaultSettings.copy(maxWaitMs = defaultSettings.maxWaitMs * 4)
      val smallerScoreDelta = initialSettings.maxScoreDelta / 2
      val shorterWaitMs = initialSettings.maxWaitMs / 2
      val newSettings = initialSettings.copy(smallerScoreDelta , shorterWaitMs)
      val testPlayer = player1
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe, settings = initialSettings)

      wPoolUnderTest ! NewWaitingPlayer(testPlayer)
      matcherProbe.expectMessageType[FindMatchRequest] // just consuming message observation
      matcherProbe.expectMessageType[StatusUpdate]

      waitForInternalEffectsMs(initialSettings.maxWaitMs / 4)

      wPoolUnderTest ! UpdateSettings(newSettings)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(_, settings) if settings.maxScoreDelta == newSettings.maxScoreDelta => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      // no immediate retries, as the new maxScoreDelta is less
      matcherProbe.expectNoMessage((newSettings.maxScoreDelta - initialSettings.maxWaitMs / 4).millis)

      matcherProbe.fishForMessage(durationFishing){ // the new (shorter) maxWaitMs have passed
        case FindMatchRequest(wPlayer) if wPlayer.player == testPlayer && wPlayer.isOvertime => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "On UpdateSettings, if both maxScoreDelta and maxWaitMs are changed, " +
    "where (new maxScoreDelta) > (old maxScoreDelta) and (new maxWaitMs) > (old maxWaitMs)" should {
    "update the settings for matching, update the timers of `WaitingPlayer`s and retry to match all with bigger maxScoreDelta" in {
      val initialSettings = defaultSettings
      val biggerScoreDelta = initialSettings.maxScoreDelta * 2
      val longerWaitMs = initialSettings.maxWaitMs * 2
      val newSettings = initialSettings.copy(biggerScoreDelta, longerWaitMs)
      val testPlayer = player1
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe, settings = initialSettings)

      wPoolUnderTest ! NewWaitingPlayer(testPlayer)
      matcherProbe.expectMessageType[FindMatchRequest] // just consuming message observation
      matcherProbe.expectMessageType[StatusUpdate]

      waitForInternalEffectsMs(initialSettings.maxWaitMs)

      wPoolUnderTest ! UpdateSettings(newSettings)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(_, settings) if settings.maxScoreDelta == newSettings.maxScoreDelta => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){ //retry based on bigger score delta
        case FindMatchRequest(wPlayer) if wPlayer.player == testPlayer && !wPlayer.isOvertime => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
      matcherProbe.expectMessageType[StatusUpdate] // just consuming message observation

      // waiting for the updated (longer) maxWaitMs to complete
      matcherProbe.expectNoMessage((newSettings.maxScoreDelta - initialSettings.maxWaitMs / 2).millis)

      matcherProbe.fishForMessage(durationFishing){ // the new (longer) maxWaitMs have passed
        case FindMatchRequest(wPlayer) if wPlayer.player == testPlayer && wPlayer.isOvertime => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "On UpdateSettings, if the new settings values are the same" should {
    "do nothing" in {
      val initialSettings = defaultSettings
      val testPlayer = player1
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe, settings = initialSettings)

      wPoolUnderTest ! NewWaitingPlayer(testPlayer)
      matcherProbe.expectMessageType[FindMatchRequest] // just consuming message observation
      matcherProbe.expectMessageType[StatusUpdate]

      matcherProbe.fishForMessage(aBitLongerThan(initialSettings.maxWaitMs)){
        case FindMatchRequest(wPlayer) if wPlayer.player == testPlayer && wPlayer.isOvertime => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
      matcherProbe.expectMessageType[StatusUpdate]

      wPoolUnderTest ! UpdateSettings(initialSettings)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(_, settings) if settings.maxScoreDelta == initialSettings.maxScoreDelta => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.expectNoMessage((initialSettings.maxScoreDelta * 2).millis)
    }
  }

  "On MatchNotFound(key)" should {
    "keep the player in the queue" in {
      val testPlayer = player1
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe)

      wPoolUnderTest ! NewWaitingPlayer(testPlayer)

      wPoolProbe.fishForMessage(durationFishing){
        case MatchNotFound(_) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(wPlayers, _) if wPlayers.head.player == testPlayer => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "MatchNotFoundSendStatus(key, requester)" should {
    "keep the player in the queue and send StatusSnapshot(waitingPlayers, settings) to Matcher for next match computation" in {
      val testPlayer1 = player1
      val testPlayer2 = player2.makeNonMatchingTo(player1)
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe)

      wPoolUnderTest ! NewWaitingPlayer(testPlayer1)
      wPoolUnderTest ! NewWaitingPlayer(testPlayer2)

      wPoolProbe.fishForMessage(durationFishing){
        case MatchNotFoundSendStatus(_, _) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(waitingPlayers, _) =>
          waitingPlayers.toSeq.map(_.player) should contain allElementsOf Seq(testPlayer1, testPlayer2)
          FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "On MatchFound(key1, key2)" should {
    "stop the corresponding `PlayerTimer`s, remove the matched players from the queue and send Matched(id1, id2) to Output" in {
      val testSettings = noTimerEffectSettings
      val matchingPl1 = player1
      val matchingPl2 = player2.makeMatchingTo(player1)
      val testedPlayers = Seq(matchingPl1, matchingPl2)
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val outputProbe: TestProbe[OutputCommand] = testKit.createTestProbe[OutputCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe, outputProbe, testSettings)

      testedPlayers.foreach(p => wPoolUnderTest ! NewWaitingPlayer(p))

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      val message = wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(waitingPlayers, _) =>
          waitingPlayers.toSeq.map(_.player) should contain allElementsOf testedPlayers
          FishingOutcomes.complete
        case _ => FishingOutcomes.continueAndIgnore
      }
      val waitingPlayers: Set[WaitingPlayer] = message.asInstanceOf[Seq[StatusSnapshot]].head.waitingPlayers

      wPoolProbe.fishForMessage(durationFishing){
        case MatchFound(key1, key2) =>
          val foundPlayers = waitingPlayers.filter(wp => wp.key == key1 || wp.key == key2).map(_.player)
           foundPlayers should contain allElementsOf testedPlayers
          FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.expectTerminated(waitingPlayers.head.timer)
      wPoolProbe.expectTerminated(waitingPlayers.last.timer)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(waitingPlayers, _) if waitingPlayers.isEmpty => FishingOutcomes.complete
        case _ => FishingOutcomes.continueAndIgnore
      }

      outputProbe.expectMessage(Matched(matchingPl1.id, matchingPl2.id))
    }
  }

  "On MatchFoundSendStatus(key1, key2, requester)" should {
    "stop the corresponding `PlayerTimer`s, remove the matched players from the queue, send Matched(id1, id2) to Output " +
      "and send StatusSnapshot(waitingPlayers, settings) to Matcher for next match computation" in {
      val testSettings = noTimerEffectSettings
      val matchingPl1 = player1
      val matchingPl2 = player3.makeMatchingTo(player1)
      val nonMatchingPl = player2.makeNonMatchingTo(player1)
      val testedPlayers = Seq(matchingPl1, nonMatchingPl, matchingPl2)
      val matcherProbe: TestProbe[MatcherCommand] = testKit.createTestProbe[MatcherCommand]()
      val wPoolProbe: TestProbe[WaitingPoolCommand] = testKit.createTestProbe[WaitingPoolCommand]()
      val outputProbe: TestProbe[OutputCommand] = testKit.createTestProbe[OutputCommand]()
      val wPoolUnderTest = monitoredWaitingPool(wPoolProbe, matcherProbe, outputProbe, testSettings)

      testedPlayers.foreach(p => wPoolUnderTest ! NewWaitingPlayer(p))

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      val message = wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(waitingPlayers, _) =>
          waitingPlayers.toSeq.map(_.player) should contain allElementsOf testedPlayers
          FishingOutcomes.complete
        case _ => FishingOutcomes.continueAndIgnore
      }
      val waitingPlayers: Set[WaitingPlayer] = message.asInstanceOf[Seq[StatusSnapshot]].head.waitingPlayers

      wPoolProbe.fishForMessage(durationFishing){
        case MatchFoundSendStatus(key1, key2, _) =>
          val foundPlayers = waitingPlayers.filter(wp => wp.key == key1 || wp.key == key2).map(_.player)
          foundPlayers should contain allElementsOf Seq(matchingPl1, matchingPl2)
          FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      val timersOfMatched = waitingPlayers.filter(wp => wp.player == matchingPl1 || wp.player == matchingPl2).map(_.timer)
      timersOfMatched should have size 2
      wPoolProbe.expectTerminated(timersOfMatched.head)
      wPoolProbe.expectTerminated(timersOfMatched.last)

      wPoolUnderTest ! StatusSnapshotAsk(wPoolProbe.ref)
      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshot(waitingPlayers, _) =>
          waitingPlayers.toSeq.map(_.player) should contain only nonMatchingPl
          FishingOutcomes.complete
        case _ => FishingOutcomes.continueAndIgnore
      }

      outputProbe.expectMessage(Matched(matchingPl1.id, matchingPl2.id))
    }
  }

}
