package pixelcrack.speed.matching.actors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.Behavior
import org.scalatest.Suites
import org.scalatest.wordspec.AnyWordSpecLike
import pixelcrack.speed.matching.actors.PlayerRepo.{FoundAllById, FoundById}
import pixelcrack.speed.matching.actors.helpers.{ActorSpecHelper, TestPlayerRepo}
import pixelcrack.speed.model.Player

class PlayerRepoSuites extends Suites (
  new GenericPlayerRepoSpec(ps => TestPlayerRepo(ps)),
  new MapPlayerRepoSpec(ps => MapPlayerRepo(ps))
)

class GenericPlayerRepoSpec(val init: Seq[Player] => Behavior[PlayerRepo.Query]) extends PlayerRepoSpec {
  "When receiving an Update(playerId, player) message, if the operation has failed" should {
    "respond to the sender with the Error message describing the failure" in {
      val newPlayer = TestPlayerRepo.updateErrorPlayer(player1)
      val requesterProbe = testKit.createTestProbe[PlayerRepo.Result]()
      val playerRepoEmpty = playerRepoRecognisingAsValid()
      val playerRepoUnderTest = testKit.spawn(playerRepoEmpty)

      playerRepoUnderTest  ! PlayerRepo.Update(newPlayer.id, newPlayer, requesterProbe.ref)

      requesterProbe.expectMessageType[PlayerRepo.Error]
    }
  }
}

class MapPlayerRepoSpec(val init: Seq[Player] => Behavior[PlayerRepo.Query]) extends PlayerRepoSpec


trait PlayerRepoSpec extends ScalaTestWithActorTestKit with ActorSpecHelper with AnyWordSpecLike {

  def init: Seq[Player] => Behavior[PlayerRepo.Query]

  def playerRepoRecognisingAsValid(players: Player*): Behavior[PlayerRepo.Query] = init(players)

  "When receiving a FindById(playerId) message, with a valid playerId" should {
    "respond to the sender with the FoundById(player)" in {
      val validPlayer = player1
      val requesterProbe = testKit.createTestProbe[PlayerRepo.Result]()
      val playerRepoForOneValid = playerRepoRecognisingAsValid(validPlayer)
      val playerRepoUnderTest = testKit.spawn(playerRepoForOneValid)

      playerRepoUnderTest ! PlayerRepo.FindById(validPlayer.id, requesterProbe.ref)

      requesterProbe.expectMessage(FoundById(validPlayer))
    }
  }

  "When receiving a FindById(playerId) message, with an invalid playerId" should {
    "respond to the sender with the Error message describing the failure" in {
      val invalidPlayer = player1
      val requesterProbe = testKit.createTestProbe[PlayerRepo.Result]()
      val playerRepoNoneValid = playerRepoRecognisingAsValid()
      val playerRepoUnderTest = testKit.spawn(playerRepoNoneValid)

      playerRepoUnderTest  ! PlayerRepo.FindById(invalidPlayer.id, requesterProbe.ref)

      requesterProbe.expectMessageType[PlayerRepo.Error]
    }
  }

  "When receiving a FindAllById(playerId) message, with all valid playerId's" should {
    "respond to the sender with the FoundAllById(winnerId, loserId)" in {
      val winner = player1
      val loser = player2
      val requesterProbe = testKit.createTestProbe[PlayerRepo.Result]()
      val playerRepoForWinnerAndLoser = playerRepoRecognisingAsValid(winner, loser)
      val playerRepoUnderTest = testKit.spawn(playerRepoForWinnerAndLoser)

      playerRepoUnderTest  ! PlayerRepo.FindAllById(winner.id, loser.id, requesterProbe.ref)

      requesterProbe.expectMessage(FoundAllById(winner, loser))
    }
  }

  "When receiving a FindAllById(playerId) message, with an invalid playerId" should {
    "respond to the sender with the Error message describing the failure" in {
      val winner = player1
      val loser = player2
      val requesterProbe = testKit.createTestProbe[PlayerRepo.Result]()
      val playerRepoForWinnerOnly = playerRepoRecognisingAsValid(winner)
      val playerRepoUnderTest = testKit.spawn(playerRepoForWinnerOnly)

      playerRepoUnderTest  ! PlayerRepo.FindAllById(winner.id, loser.id, requesterProbe.ref)

      requesterProbe.expectMessageType[PlayerRepo.Error]
    }
  }

  "When receiving an Update(playerId, player) message, for an existent playerId" should {
    "update the Player and respond to the sender with the UpdateSuccess(updatedPlayer)" in {
      val existentPlayer = player1
      val updatedPlayer = existentPlayer.copy(score = existentPlayer.score + 1)
      val requesterProbe = testKit.createTestProbe[PlayerRepo.Result]()
      val playerRepoForExistent = playerRepoRecognisingAsValid(existentPlayer)
      val playerRepoUnderTest = testKit.spawn(playerRepoForExistent)

      playerRepoUnderTest ! PlayerRepo.Update(updatedPlayer.id, updatedPlayer, requesterProbe.ref)

      requesterProbe.expectMessage(PlayerRepo.UpdateSuccess(updatedPlayer))
    }
  }

  "When receiving a Update(playerId, player) message, for a non-existent playerId" should {
    "create the Player and respond to the sender with the UpdateSuccess(updatedPlayer)" in {
      val newPlayer = player1
      val requesterProbe = testKit.createTestProbe[PlayerRepo.Result]()
      val playerRepoEmpty = playerRepoRecognisingAsValid()
      val playerRepoUnderTest = testKit.spawn(playerRepoEmpty)

      playerRepoUnderTest  ! PlayerRepo.Update(newPlayer.id, newPlayer, requesterProbe.ref)

      requesterProbe.expectMessage(PlayerRepo.UpdateSuccess(newPlayer))
    }
  }

}