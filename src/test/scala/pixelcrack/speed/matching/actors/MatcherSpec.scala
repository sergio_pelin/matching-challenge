package pixelcrack.speed.matching.actors

import akka.actor.testkit.typed.scaladsl.{FishingOutcomes, ScalaTestWithActorTestKit}
import akka.actor.typed.scaladsl.Behaviors
import org.scalatest.wordspec.AnyWordSpecLike
import pixelcrack.speed.matching.actors.Matcher._
import pixelcrack.speed.matching.actors.WaitingPool._
import pixelcrack.speed.matching.actors.helpers.{ActorSpecHelper, MonitoredMatcher}

import scala.collection.immutable.SortedSet

class MatcherSpec extends ScalaTestWithActorTestKit with ActorSpecHelper with AnyWordSpecLike {

  "In `ready` mode: on receiving `FindMatchRequest(wPlayer)`" should {
    "send `StatusSnapshotAsk` to the `WaitingPool`" in {
      val wPoolProbeName = "test-waiting-pool"
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand](wPoolProbeName)
      val wPlayer = wPlayerFor(player1, wPoolProbe.ref)
      val matcherUnderTest = testKit.spawn(Matcher(wPoolProbe.ref, defaultSettings.maxWaitMs))

      matcherUnderTest ! FindMatchRequest(wPlayer)

      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshotAsk(ref) if ref.path.name.contains(wPoolProbeName) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "In `ready` mode, with ONE ONLY `WaitingPlayer`: on receiving `FindMatchRequest(wPlayer)`" should {
    "send MatchNotFound(key) to the `WaitingPool``" in {
      val testSettings = noTimerEffectSettings
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val wPlayer = wPlayerFor(player1, wPoolProbe.ref, testSettings.maxWaitMs)
      val onePlayerOnly = SortedSet(wPlayer)
      val mockWPool = mockWaitingPool(wPoolProbe, Behaviors.receiveMessage[WaitingPoolCommand] {
        case StatusSnapshotAsk(requester) =>
          requester ! StatusSnapshot(onePlayerOnly, testSettings)
          Behaviors.same
        case _ => Behaviors.unhandled
      })

      val matcherProbe = testKit.createTestProbe[MatcherCommand]()

      val matcherUnderTest = testKit.spawn(MonitoredMatcher(matcherProbe.ref, mockWPool))

      matcherUnderTest ! FindMatchRequest(wPlayer)

      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshotAsk(_) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(wPlayers, _) if wPlayers == onePlayerOnly => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchNotFound(key) if key == wPlayer.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "In `ready` mode, with two NON-MATCHING players: on receiving `FindMatchRequest(wPlayer)`" should {
    "send `MatchNotFoundSendStatus(key, ref)` and then MatchNotFound(key) to the `WaitingPool`" in {
      val testSettings = noTimerEffectSettings
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val wPlayer1 = wPlayerFor(player1, wPoolProbe.ref, testSettings.maxWaitMs)
      val wPlayer2 = wPlayerFor(player2.makeNonMatchingTo(player1), wPoolProbe.ref, testSettings.maxWaitMs, delay = 100)
      val twoNonMatchingPlayers = SortedSet(wPlayer1, wPlayer2)
      val mockWPool = mockWaitingPool(wPoolProbe, Behaviors.receiveMessage[WaitingPoolCommand] {
        case StatusSnapshotAsk(requester) =>
          requester ! StatusSnapshot(twoNonMatchingPlayers, testSettings)
          Behaviors.same
        case MatchNotFoundSendStatus(_, requester) =>
          requester ! StatusSnapshot(twoNonMatchingPlayers, testSettings)
          Behaviors.same
        case _ => Behaviors.unhandled
      })
      val matcherProbe = testKit.createTestProbe[MatcherCommand]()

      val matcherUnderTest = testKit.spawn(MonitoredMatcher(matcherProbe.ref, mockWPool))

      matcherUnderTest ! FindMatchRequest(wPlayer1)
      matcherUnderTest ! FindMatchRequest(wPlayer2)

      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshotAsk(_) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(wPlayers, _) if wPlayers == twoNonMatchingPlayers => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchNotFoundSendStatus(key, _) if key == wPlayer1.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchNotFound(key) if key == wPlayer2.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "In `ready` mode, with two MATCHING players: on receiving `FindMatchRequest(wPlayer)`" should {
    "send `MatchFound(key1, key2)` to the `WaitingPool`" in {
      val testSettings = noTimerEffectSettings
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val wPlayer1 = wPlayerFor(player1, wPoolProbe.ref, testSettings.maxWaitMs)
      val wPlayer2 = wPlayerFor(player2.makeMatchingTo(player1), wPoolProbe.ref, testSettings.maxWaitMs, delay = 100)
      val twoMatchingPlayers = SortedSet(wPlayer1, wPlayer2)
      val matcherProbe = testKit.createTestProbe[MatcherCommand]()
      val mockWPool = mockWaitingPool(wPoolProbe, Behaviors.receiveMessage[WaitingPoolCommand] {
        case StatusSnapshotAsk(requester) =>
          requester ! StatusSnapshot(twoMatchingPlayers, testSettings)
          Behaviors.same
        case _ => Behaviors.unhandled
      })

      val matcherUnderTest = testKit.spawn(MonitoredMatcher(matcherProbe.ref, mockWPool))

      matcherUnderTest ! FindMatchRequest(wPlayer1)
      matcherUnderTest ! FindMatchRequest(wPlayer2)

      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshotAsk(_) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(wPlayers, _) if wPlayers == twoMatchingPlayers => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchFound(key1, key2) if key1 == wPlayer1.key && key2 == wPlayer2.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.expectNoMessage
    }
  }

  "In `ready` mode, with two MATCHING and one NON-MATCHING players: on receiving `FindMatchRequest(wPlayer)`" should {
    "send `MatchFoundSendStatus(key1, key2, ref)` and then MatchNotFound(key) to the `WaitingPool`" in {
      val testSettings = noTimerEffectSettings
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val wPlayer1 = wPlayerFor(player1, wPoolProbe.ref, testSettings.maxWaitMs)
      val wPlayer2 = wPlayerFor(player2.makeMatchingTo(player1), wPoolProbe.ref, testSettings.maxWaitMs, delay = 200)
      val wPlayerNonMatch = wPlayerFor(player3.makeNonMatchingTo(player1), wPoolProbe.ref, testSettings.maxWaitMs, delay = 100)
      val mixedPlayers = SortedSet(wPlayer1, wPlayer2, wPlayerNonMatch)
      val matcherProbe = testKit.createTestProbe[MatcherCommand]()
      val mockWPool = mockWaitingPool(wPoolProbe, Behaviors.receiveMessage[WaitingPoolCommand] {
        case StatusSnapshotAsk(requester) =>
          requester ! StatusSnapshot(mixedPlayers, testSettings)
          Behaviors.same
        case MatchFoundSendStatus(key1, key2, requester) =>
          val removedMatch = mixedPlayers.filterNot(wp => wp.key == key1 || wp.key == key2)
          requester ! StatusSnapshot(removedMatch , testSettings)
          Behaviors.same
        case _ => Behaviors.unhandled
      })

      val matcherUnderTest = testKit.spawn(MonitoredMatcher(matcherProbe.ref, mockWPool))

      matcherUnderTest ! FindMatchRequest(wPlayer1)
      matcherUnderTest ! FindMatchRequest(wPlayerNonMatch)
      matcherUnderTest ! FindMatchRequest(wPlayer2)

      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshotAsk(_) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(wPlayers, _) if wPlayers == mixedPlayers => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchFoundSendStatus(key1, key2, _) if key1 == wPlayer1.key && key2 == wPlayer2.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(wPlayers, _) if wPlayers.contains(wPlayerNonMatch) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchNotFound(key) if key == wPlayerNonMatch.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }
  }

  "In `ready` mode, with three NON-MATCHING OVERTIME players: on receiving `FindMatchRequest(wPlayer)`" should {
    "compute the match regardless of the `maxScoreDelta`" in {
      val testSettings = noTimerEffectSettings
      val wPoolProbe = testKit.createTestProbe[WaitingPoolCommand]()
      val wPlayer1 = wPlayerFor(player1, wPoolProbe.ref, testSettings.maxWaitMs).copy(isOvertime = true)
      val wPlayer2 = wPlayerFor(player2.makeNonMatchingTo(player1), wPoolProbe.ref, testSettings.maxWaitMs, delay = 200).copy(isOvertime = true)
      val wPlayer3 = wPlayerFor(player3.makeNonMatchingTo(player2), wPoolProbe.ref, testSettings.maxWaitMs, delay = 100).copy(isOvertime = true)
      val threeNonMatchingOvertimePlayers = SortedSet(wPlayer1, wPlayer2, wPlayer3)
      val mockWPool = mockWaitingPool(wPoolProbe, Behaviors.receiveMessage[WaitingPoolCommand] {
        case StatusSnapshotAsk(requester) =>
          requester ! StatusSnapshot(threeNonMatchingOvertimePlayers, testSettings)
          Behaviors.same
        case MatchFoundSendStatus(key1, key2, requester) =>
          val removedMatch = threeNonMatchingOvertimePlayers.filterNot(wp => wp.key == key1 || wp.key == key2)
          requester ! StatusSnapshot(removedMatch , testSettings)
          Behaviors.same
        case MatchNotFoundSendStatus(_, requester) =>
          requester ! StatusSnapshot(threeNonMatchingOvertimePlayers, testSettings)
          Behaviors.same
        case _ => Behaviors.unhandled
      })
      val matcherProbe = testKit.createTestProbe[MatcherCommand]()

      val matcherUnderTest = testKit.spawn(MonitoredMatcher(matcherProbe.ref, mockWPool))

      matcherUnderTest ! FindMatchRequest(wPlayer1)
      matcherUnderTest ! FindMatchRequest(wPlayer3)
      matcherUnderTest ! FindMatchRequest(wPlayer2)

      wPoolProbe.fishForMessage(durationFishing){
        case StatusSnapshotAsk(_) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(wPlayers, _) if wPlayers == threeNonMatchingOvertimePlayers => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchFoundSendStatus(key1, key2, _) if key1 == wPlayer1.key && key2 == wPlayer2.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      matcherProbe.fishForMessage(durationFishing){
        case StatusUpdate(wPlayers, _) if wPlayers.contains(wPlayer3) => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }

      wPoolProbe.fishForMessage(durationFishing){
        case MatchNotFound(key) if key == wPlayer3.key => FishingOutcomes.complete
        case _ => FishingOutcomes.continue
      }
    }

  }
}
