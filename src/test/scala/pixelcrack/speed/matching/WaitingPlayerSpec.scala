package pixelcrack.speed.matching

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike
import pixelcrack.speed.matching.actors.WaitingPlayerTimer.{PlayerTimerCommand, UpdateTimer}
import pixelcrack.speed.matching.actors.helpers.ActorSpecHelper

class WaitingPlayerSpec extends ScalaTestWithActorTestKit with AnyWordSpecLike with ActorSpecHelper  {

  "On `withUpdatedTimer`" should {
    "send an UpdateTimer(newWaitMs) message to its timer" in {
      val timerProbe = testKit.createTestProbe[PlayerTimerCommand]()
      val newWaitTimeMs = defaultSettings.maxWaitMs

      val wPlayerUnderTest = WaitingPlayer(player1, waitKeyFor(player1), timerProbe.ref)

      wPlayerUnderTest.withUpdatedTimer(newWaitTimeMs)

      timerProbe.expectMessage(UpdateTimer(newWaitTimeMs))
    }
  }

}
