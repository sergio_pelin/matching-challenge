package pixelcrack.speed

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem}
import pixelcrack.speed.matching.actors.Output.OutputCommand
import pixelcrack.speed.matching.actors.PlayerProvider.{NewGameResult, NewPlayer}
import pixelcrack.speed.matching.actors.WaitingPool.UpdateSettings
import pixelcrack.speed.matching.actors._
import pixelcrack.speed.model.{Player, PlayerId, Settings}



object MatchingServiceApp extends App {

  // This would be in the application.conf
  val initialSettings: Settings = Settings(0.25, 6000)

  // This would be in the persistence
  val initialPlayers: Seq[Player] = Seq(
    Player(PlayerId(1), 0.0, Set.empty),
    Player(PlayerId(2), 0.0, Set.empty),
    Player(PlayerId(3), 0.0, Set.empty),
    Player(PlayerId(4), 0.0, Set.empty)
  )

  sealed trait SpeedAppCommand
  final case class PlayerInput(id: Long) extends SpeedAppCommand
  final case class GameInput(winner: Long, loser: Long) extends SpeedAppCommand
  final case class ConfigInput(maxScoreDelta: Double, maxWaitMs: Int) extends SpeedAppCommand
  case object Stop extends SpeedAppCommand

  private val behavior = Behaviors.setup[SpeedAppCommand]{ context =>

    val output: ActorRef[OutputCommand] = context.spawn(Output(), "output-manager")

    val playerRepo: ActorRef[PlayerRepo.Query] = context.spawn(MapPlayerRepo(initialPlayers), "player-repo")

    val waitingPool: ActorRef[WaitingPool.WaitingPoolCommand] =
      context.spawn(WaitingPool(initialSettings, output), "waiting-pool")

    val playerProvider = context.spawn(PlayerProvider(playerRepo, waitingPool), "player-provider")

    Behaviors.receiveMessage{

      case PlayerInput(id) =>
        playerProvider ! NewPlayer(PlayerId(id))
        Behaviors.same

      case GameInput(winner: Long, loser: Long) =>
        playerProvider ! NewGameResult(PlayerId(winner), PlayerId(loser))
        Behaviors.same

      case ConfigInput(maxScoreDelta, maxWaitMs) =>
        waitingPool ! UpdateSettings(Settings(maxScoreDelta, maxWaitMs))
        Behaviors.same

      case Stop => Behaviors.stopped
    }

  }

  val matchingService = ActorSystem(behavior, "matching-service-system")


  //=== SAMPLE INPUT ===
  // the thread sleeps just simulate the uneven arrival of data

  matchingService ! PlayerInput(1)

  Thread.sleep(3000)

  matchingService ! PlayerInput(2)

  Thread.sleep(1000)

  matchingService ! PlayerInput(3)

  Thread.sleep(2000)

  matchingService ! PlayerInput(4)

  Thread.sleep(1000)

  matchingService ! GameInput(1, 2)
  matchingService ! GameInput(4, 3)

  Thread.sleep(2000)


  matchingService.terminate()

}
