package pixelcrack.speed.matching

import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import Math.abs

object Utils {

  def actorName(name: String): String = URLEncoder.encode(name, StandardCharsets.UTF_8.name)

  implicit class DoubleUtils(number: Double) {

    def normalisedTo(decimals: Int): Double =
      BigDecimal(number).setScale(decimals, BigDecimal.RoundingMode.HALF_UP).toDouble

    def isNotBiggerThan(other: Double): Boolean = {
      val decimals = other.toString.dropWhile(_ != '.').length - 1
      normalisedTo(decimals) <= other
    }

    def absDiff(other: Double): Double = abs(abs(number) - abs(other))
  }

}
