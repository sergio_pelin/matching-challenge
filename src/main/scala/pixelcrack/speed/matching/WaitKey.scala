package pixelcrack.speed.matching

import pixelcrack.speed.model.Player

object WaitKey {

  def apply(arrivalTime: Long, player: Player): WaitKey =
    new WaitKey(s"$arrivalTime-${player.hashCode}")
}

/**
 * A key to be used in sorted data structures,
 * providing an order based on arrival time.
 */
class WaitKey private(val value: String) extends Ordered[WaitKey] {

  override def compare(that: WaitKey): Int = {
    if (value < that.value) -1
    else if (value == that.value) 0
    else 1
  }

  override def toString: String = value
}
