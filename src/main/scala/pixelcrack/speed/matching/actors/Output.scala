package pixelcrack.speed.matching.actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import pixelcrack.speed.model.PlayerId


object Output {

  sealed trait OutputCommand

  final case class Matched(player1: PlayerId, player2: PlayerId) extends OutputCommand

  def apply(): Behavior[OutputCommand] = output

  private def output: Behavior[OutputCommand] = {
    Behaviors.receive { (context, message) =>
      message match {

        case Matched(player1, player2) =>

          context.log.info(s"*** Matched(${player1.value}, ${player2.value})")

          Behaviors.same
      }
    }
  }
}