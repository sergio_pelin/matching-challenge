package pixelcrack.speed.matching.actors

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import pixelcrack.speed.matching.WaitKey
import pixelcrack.speed.matching.actors.WaitingPlayerTimer._
import pixelcrack.speed.matching.actors.WaitingPool.{TimeIsUpFor, WaitingPoolCommand}

import scala.concurrent.duration.DurationLong

object WaitingPlayerTimer {

  private case object TimerKey

  sealed trait PlayerTimerCommand
  private case object TimeIsUp extends PlayerTimerCommand
  final case class UpdateTimer(waitMs: Long) extends PlayerTimerCommand

  def apply(
    waitKey: WaitKey,
    arrivalTimeMs: Long,
    waitMs: Long,
    waitingPool: ActorRef[WaitingPoolCommand]): Behavior[PlayerTimerCommand] = {
    Behaviors.setup { context =>
      Behaviors.withTimers { timers =>
        timers.startSingleTimer(TimerKey, TimeIsUp, waitMs.millis)
        context.log.info(s"*** Timer started for waiting player [$waitKey]")
        new WaitingPlayerTimer(context, waitKey, arrivalTimeMs, waitingPool, timers).waiting
      }
    }
  }
}

/**
 * Provides the timer which is assigned to each `Player`
 * that joins the `WaitingPool` and becomes a `WaitingPlayer`.
 */
class WaitingPlayerTimer private(
  context: ActorContext[PlayerTimerCommand],
  waitKey: WaitKey,
  arrivalTimeMs: Long,
  waitingPool: ActorRef[WaitingPoolCommand],
  timers: TimerScheduler[PlayerTimerCommand]) {

  private def waiting: Behavior[PlayerTimerCommand] = {
    Behaviors.receiveMessage{

      case TimeIsUp =>
        context.log.info(s"*** Time is up for waiting player p$waitKey]")
        waitingPool ! TimeIsUpFor(waitKey)
        Behaviors.same

      case UpdateTimer(newWaitTimeMs) =>
        context.log.info(s"*** Updating timer for waiting player [$waitKey]. New wait time: $newWaitTimeMs")
        updateTimer(newWaitTimeMs)
    }
  }

  private def updateTimer(newWaitMs: Long): Behavior[PlayerTimerCommand] = {
    val newStopMs = arrivalTimeMs + newWaitMs
    val timeLeftMs = newStopMs - System.currentTimeMillis()
    if (timeLeftMs <= 0) context.self ! TimeIsUp
    else timers.startSingleTimer(TimerKey, TimeIsUp, timeLeftMs.millis)
    new WaitingPlayerTimer(context, waitKey, arrivalTimeMs, waitingPool, timers).waiting
  }
}
