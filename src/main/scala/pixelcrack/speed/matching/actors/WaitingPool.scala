package pixelcrack.speed.matching.actors

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import pixelcrack.speed.matching
import pixelcrack.speed.matching.actors.Matcher.{FindMatchRequest, MatcherCommand}
import pixelcrack.speed.matching.actors.Output.{Matched, OutputCommand}
import pixelcrack.speed.matching.actors.WaitingPlayerTimer.PlayerTimerCommand
import pixelcrack.speed.matching.Utils.actorName
import pixelcrack.speed.matching.actors.WaitingPool._
import pixelcrack.speed.matching.{WaitKey, WaitingPlayer}
import pixelcrack.speed.model.{Player, Settings}

import scala.collection.immutable.{SortedMap, SortedSet}


object WaitingPool {

  sealed trait WaitingPoolCommand

  sealed trait ActionCommand extends WaitingPoolCommand
  final case class NewWaitingPlayer(player: Player) extends ActionCommand
  final case class TimeIsUpFor(key: WaitKey) extends ActionCommand
  final case class UpdateSettings(settings: Settings) extends ActionCommand

  sealed trait ProcessCommand extends WaitingPoolCommand
  final case class MatchNotFound(key: WaitKey) extends ProcessCommand
  final case class MatchNotFoundSendStatus(key: WaitKey, replyTo: ActorRef[StatusCommand]) extends ProcessCommand
  final case class MatchFound(key1: WaitKey, key2: WaitKey) extends ProcessCommand
  final case class MatchFoundSendStatus(key1: WaitKey, key2: WaitKey, replyTo: ActorRef[StatusCommand]) extends ProcessCommand

  sealed trait StatusCommand extends WaitingPoolCommand
  final case class StatusSnapshotAsk(replayTo: ActorRef[StatusCommand]) extends StatusCommand
  final case class StatusSnapshot(waitingPlayers: SortedSet[WaitingPlayer], settings: Settings) extends StatusCommand

  def apply(
    settings: Settings,
    outputManager: ActorRef[OutputCommand],
    waitingPlayers: SortedMap[WaitKey, WaitingPlayer] = SortedMap.empty): Behavior[WaitingPoolCommand] = {

    Behaviors.setup { context =>
      val matcher = context.spawn(Matcher(context.self, settings.maxScoreDelta), name = "matcher")
      new WaitingPool(context, settings, waitingPlayers, matcher, outputManager).running
    }
  }

  implicit class WaitingPlayersOpts(waitingPlayers: SortedMap[WaitKey, WaitingPlayer]){
    def asSortedSet: SortedSet[WaitingPlayer] = SortedSet.empty[WaitingPlayer] ++ waitingPlayers.values
  }
}

/**
 * Stores the `Player`s waiting for a match, sending for them match requests to the `Matcher`:
 * 1. in the order of arrival time
 * 2. when the wait time is over
 * 3. when the settings changes might require it (e.g. increased max score delta)
 *
 * @param context Actor's context
 * @param settings Currently applied settings
 * @param waitingPlayers The full collection of the `WaitingPlayer`s
 * @param matcher The child-actor `Matcher`
 * @param outputManager The actor to receive the results of matching
 */
class WaitingPool protected(
  context: ActorContext[WaitingPoolCommand],
  settings: Settings,
  waitingPlayers: SortedMap[WaitKey, WaitingPlayer],
  matcher: ActorRef[MatcherCommand],
  outputManager: ActorRef[OutputCommand]) {

  import WaitingPool._

  protected def running: Behavior[WaitingPoolCommand] = {
    Behaviors.receiveMessage {

      case command: ActionCommand => onActionCommand(command)

      case command: ProcessCommand => onProcessCommand(command)

      case command: StatusCommand => onStatusCommand(command)

    }
  }

  private def onActionCommand(command: ActionCommand): Behavior[WaitingPoolCommand] = command match {

    case NewWaitingPlayer(player) =>
      context.log.info(s"*** Adding new waiting player: $player")
      addPlayerAndSendMatchRequest(player)

    case TimeIsUpFor(key) =>
      context.log.info(s"*** Marking as overtime the waiting player [$key]")
      markAsOvertimeAndSendMatchRequest(key)

    case UpdateSettings(s @ Settings(delta, waitMs)) if delta != settings.maxScoreDelta && waitMs != settings.maxWaitMs =>
      context.log.info(s"*** Updating settings: $s")
      if (delta > settings.maxScoreDelta) {
        this.withUpdatedSettings(s).withUpdatedWaitingPlayerTimers(waitMs).retryMatchingAllWaitingPlayers
      } else {
        this.withUpdatedSettings(s).withUpdatedWaitingPlayerTimers(waitMs).running
      }

    case UpdateSettings(s @ Settings(delta, _)) if delta != settings.maxScoreDelta =>
      context.log.info(s"*** Updating settings: $s")
      if (delta > settings.maxScoreDelta) {
        this.withUpdatedSettings(s).retryMatchingAllWaitingPlayers
      } else {
        this.withUpdatedSettings(s).running
      }

    case UpdateSettings(s @ Settings(_, waitMs)) if waitMs != settings.maxWaitMs =>
      context.log.info(s"*** Updating settings: $s")
      this.withUpdatedSettings(s).withUpdatedWaitingPlayerTimers(waitMs).running

    case UpdateSettings(Settings(_, _)) =>
      context.log.info(s"*** Updating settings with same values. Skipping.")
      Behaviors.same
  }

  private def onProcessCommand(command: ProcessCommand): Behavior[WaitingPoolCommand] = {

    def onMatchNotFound(key: WaitKey, requester: Option[ActorRef[StatusCommand]] = None): Behavior[WaitingPoolCommand] = {
      requester.foreach(_ ! StatusSnapshot(waitingPlayers.asSortedSet, settings))
      context.log.info(s"*** Match not found for the waiting player [$key]")
      Behaviors.same
    }

    def onMatchFound(key1: WaitKey, key2: WaitKey, requester: Option[ActorRef[StatusCommand]] = None): Behavior[WaitingPoolCommand] = {
      context.log.info(s"*** Match found for waiting players: [$key1] - [$key2]")
      (waitingPlayers.get(key1), waitingPlayers.get(key2)) match {
        case (Some(wp1), Some(wp2)) =>
          context.stop(wp1.timer)
          context.stop(wp2.timer)
          val matchedRemovedFromWaiting = waitingPlayers -- Seq(key1, key2)
          requester.foreach(_ ! StatusSnapshot(matchedRemovedFromWaiting.asSortedSet, settings))
          outputManager ! Matched(wp1.player.id, wp2.player.id)
          this.withUpdatedPlayers(matchedRemovedFromWaiting).running

        // The next cases could happen if there was the possibility for players to stop waiting (e.g. log out)
        case (None, Some(_)) =>
          context.log.info(s"*** The player [$key1] is missing for the found match. Rematching player [$key2]")
          sendMatchRequestFor(key2)

        case (Some(_), None) =>
          context.log.info(s"*** The player [$key2] is missing for the found match. Rematching player [$key1]")
          sendMatchRequestFor(key1)

        case _ => Behaviors.same
      }
    }

    command match {

      case MatchNotFound(key) => onMatchNotFound(key)

      case MatchNotFoundSendStatus(key, requester) => onMatchNotFound(key, Some(requester))

      case MatchFound(key1, key2) => onMatchFound(key1, key2)

      case MatchFoundSendStatus(key1, key2, requester) => onMatchFound(key1, key2, Some(requester))

    }
  }

  private def onStatusCommand(command: StatusCommand): Behavior[WaitingPoolCommand] = command match {

    case StatusSnapshotAsk(requester) =>
      requester ! StatusSnapshot(waitingPlayers.asSortedSet, settings)
      Behaviors.same
  }

  private def sendMatchRequestFor(key: WaitKey): Behavior[WaitingPoolCommand] = {
    waitingPlayers.get(key).foreach(wPlayer => matcher ! FindMatchRequest(wPlayer))
    this.running
  }

  private def addPlayerAndSendMatchRequest(player: Player): Behavior[WaitingPoolCommand] = {
    val arrivalTimeMs = System.currentTimeMillis
    val waitKey: WaitKey = WaitKey(arrivalTimeMs, player)
    val timer: ActorRef[PlayerTimerCommand] = context.spawn(
      behavior = WaitingPlayerTimer(waitKey, arrivalTimeMs, settings.maxWaitMs, context.self),
      actorName(s"timer-for-player-${player.id.value}")
    )
    val newWaitingPlayer = matching.WaitingPlayer(player, waitKey, timer)
    val waitingPlayersUpdated = waitingPlayers.updated(waitKey, newWaitingPlayer)
    this.withUpdatedPlayers(waitingPlayersUpdated).sendMatchRequestFor(waitKey)
  }

  private def markAsOvertimeAndSendMatchRequest(key: WaitKey): Behavior[WaitingPoolCommand] = {
    (for {
      wPlayer <- waitingPlayers.get(key)
      wps = waitingPlayers.updated(key, wPlayer.markedOvertime)
      upd = this.withUpdatedPlayers(wps)
    } yield upd.sendMatchRequestFor(key))
      .getOrElse(Behaviors.same)
  }

  private def retryMatchingAllWaitingPlayers: Behavior[WaitingPoolCommand] = {
    waitingPlayers.values.foreach(wp => matcher ! FindMatchRequest(wp))
    this.running
  }

  private def withUpdatedPlayers(updatedWaitingPlayers: SortedMap[WaitKey, WaitingPlayer]): WaitingPool =
    new WaitingPool(
      context,
      settings,
      updatedWaitingPlayers,
      matcher,
      outputManager)

  private def withUpdatedSettings(updatedSettings: Settings): WaitingPool = new WaitingPool(
    context,
    updatedSettings,
    waitingPlayers,
    matcher,
    outputManager)

  private def withUpdatedWaitingPlayerTimers(waitMs: Long): WaitingPool = {
    val waitingPlayersUpdated = waitingPlayers.map {
      case (key, wPlayer) => (key, wPlayer.copy(isOvertime = false).withUpdatedTimer(waitMs))
    }
    this.withUpdatedPlayers(waitingPlayersUpdated)
  }
}
