package pixelcrack.speed.matching.actors

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout
import pixelcrack.speed.matching.Utils._
import pixelcrack.speed.matching._
import pixelcrack.speed.matching.actors.Matcher.{FindMatchRequest, MatcherCommand, StatusUpdate, StatusUpdateFailed}
import pixelcrack.speed.matching.actors.WaitingPool._

import scala.collection.immutable.SortedSet
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}


object Matcher {

  protected [actors] trait MatcherCommand
  final case class FindMatchRequest(wPlayer: WaitingPlayer) extends MatcherCommand
  final case class StatusUpdate(waitingPlayers: SortedSet[WaitingPlayer], maxScoreDelta: Double) extends MatcherCommand
  final case class StatusUpdateFailed(error: String) extends MatcherCommand

  def apply(waitingPool: ActorRef[WaitingPoolCommand], maxScoreDelta: Double): Behavior[MatcherCommand] = {
    Behaviors.setup { context =>
      new Matcher(context, waitingPool, SortedSet.empty, SortedSet.empty, maxScoreDelta).ready
    }
  }
}

/**
 * Computes the matches for the `WaitingPlayer`s indicated in the requests from the `waitingPool`.
 *
 * Receives match requests from the `waitingPool` and stores them to the `matchingQueue`.
 * The
 * Before every matching intent, this updates the `waitingPool` status
 * (i. e. currently `WaitingPlayer`s and the max score delta).
 * If a new match request is receive while computing a match, it is added
 * to the `matchingQueue`.
 *
 * @param context Actor's context
 * @param waitingPool The parent of this actor, which manages all the `WaitingPlayer`s
 * @param matchingQueue A `SortedSet` holding the `WaitingPlayers` requested to be matched
 * @param currentWaitingPlayers The latest complete set of `WaitingPlayer`s in the pool
 * @param currentMaxScoreDelta The latest `maxScoreDelta`
 */
class Matcher protected(
  context: ActorContext[MatcherCommand],
  waitingPool: ActorRef[WaitingPoolCommand],
  matchingQueue: SortedSet[WaitingPlayer],
  currentWaitingPlayers: SortedSet[WaitingPlayer],
  currentMaxScoreDelta: Double) {

  private implicit val timeout: Timeout = 1.second

  /**
   * The mode when the `matchingQueue` is empty and the `Matcher` is ready to receive new messages.
   */
  protected def ready: Behavior[MatcherCommand] = {
    Behaviors.receiveMessage {

      case FindMatchRequest(wPlayer) =>
        context.log.info(s"*** Match request received for waiting player [$wPlayer]. Starting to process.")
        this.withUpdatedMatchingQueue(matchingQueue + wPlayer).askStatusUpdate

      case StatusUpdate(waitingPlayers, maxScoreDelta) =>
        context.log.info(s"*** Status updated for next match.\n* Waiting players: ${waitingPlayers.mkString(", ")}" +
          s"\n* MaxScoreDelta: $maxScoreDelta")
        this.withUpdatedStatus(waitingPlayers, maxScoreDelta).matchNext

      case StatusUpdateFailed(error) => //TODO Retry strategy could be applied here
        context.log.error(s"*** Error on StatusUpdate: $error")
        Behaviors.same
    }
  }

  /**
   * The mode when being busy, i.e. processing items from the `matchingQueue`.
   */
  protected def processing: Behavior[MatcherCommand] = {
    Behaviors.receiveMessage {

      case FindMatchRequest(wPlayer) =>
        context.log.info(s"*** Match request received for waiting player [$wPlayer]. Adding to queue, process later.")
        this.withUpdatedMatchingQueue(matchingQueue + wPlayer).processing

      case StatusUpdate(waitingPlayers, maxScoreDelta) =>
        context.log.info(s"*** Status updated for next match.\n* Waiting players: ${waitingPlayers.mkString(", ")}" +
          s"\n* MaxScoreDelta: $maxScoreDelta")
        this.withUpdatedStatus(waitingPlayers, maxScoreDelta).matchNext

      case StatusUpdateFailed(error) => //TODO Retry strategy could be applied here
        context.log.error(s"*** Error on StatusUpdate: $error")
        Behaviors.same
    }
  }

  /**
   * Retrieves the latest status (relevant details used in the matching logic) from the `WaitingPool`.
   */
  private def askStatusUpdate: Behavior[MatcherCommand] = {
    context.log.info(s"*** Sending status update request to WaitingPool")
    context.ask(waitingPool, ref => StatusSnapshotAsk(ref)) {
      case Success(StatusSnapshot(waitingPlayers, settings)) =>
        StatusUpdate(waitingPlayers, settings.maxScoreDelta)
      case Failure(exception) => StatusUpdateFailed(exception.getMessage)
    }
    this.processing
  }

  /**
   * Updates the local `waitingPlayers` and the `maxScoreDelta`. Removes from `matchingQueue` the players not
   * present among the `waitingPlayers`, as they could be removed after the previous matches or just logged off.
   */
  private def withUpdatedStatus(waitingPlayersUpdated: SortedSet[WaitingPlayer], maxScoreDeltaUpdated: Double): Matcher = {
    val matchingQueueUpdated = matchingQueue.intersect(waitingPlayersUpdated)
    new Matcher(context, waitingPool, matchingQueueUpdated, waitingPlayersUpdated, maxScoreDeltaUpdated)
  }

  private def withUpdatedMatchingQueue(matchingQueueUpdated: SortedSet[WaitingPlayer]): Matcher =
    new Matcher(context, waitingPool, matchingQueueUpdated, currentWaitingPlayers, currentMaxScoreDelta)

  private def sendMatchNotFoundFor(key: WaitKey): Behavior[MatcherCommand] = {
    if (matchingQueue.nonEmpty) {
      context.ask(waitingPool, ref => MatchNotFoundSendStatus(key, ref)) {
        case Success(StatusSnapshot(waitingPlayers, settings)) => StatusUpdate(waitingPlayers, settings.maxScoreDelta)
        case Failure(exception) => StatusUpdateFailed(exception.getMessage)
      }
      this.processing
    } else {
      waitingPool ! MatchNotFound(key)
      this.ready
    }
  }

  private def sendMatchFoundFor(playerKey: WaitKey, foundMatch: WaitKey): Behavior[MatcherCommand] =
    matchingQueue.toSeq match {
      case Nil =>
        waitingPool ! MatchFound(playerKey, foundMatch)
        this.ready

      case Seq(last) if last.key == foundMatch =>
        waitingPool ! MatchFound(playerKey, foundMatch)
        this.ready

      case _ =>
        context.ask(waitingPool, ref => MatchFoundSendStatus(playerKey, foundMatch, ref)) {
          case Success(StatusSnapshot(waitingPlayers, settings)) => StatusUpdate(waitingPlayers, settings.maxScoreDelta)
          case Failure(exception) => StatusUpdateFailed(exception.getMessage)
        }
        this.processing
    }

  private def candidatesFor(wPlayer: WaitingPlayer): SortedSet[WaitingPlayer] = {
    val excluded = wPlayer.player.playedOpponents + wPlayer.player.id
    currentWaitingPlayers.filterNot(wp => excluded.contains(wp.player.id))
  }

  private def tryToMatch(wPlayer: WaitingPlayer, candidates: SortedSet[WaitingPlayer]): Behavior[MatcherCommand] = {
    val (matchKey, diff): (WaitKey, Double) =
      candidates.map(wp => (wp.key, wp.player.score.absDiff(wPlayer.player.score))).minBy(_._2)

    if (wPlayer.isOvertime || diff.isNotBiggerThan(currentMaxScoreDelta)) {
      this.withUpdatedMatchingQueue(matchingQueue.tail).sendMatchFoundFor(wPlayer.key, matchKey)
    } else {
      this.withUpdatedMatchingQueue(matchingQueue.tail).sendMatchNotFoundFor(wPlayer.key)
    }
  }

  private def matchNext: Behavior[MatcherCommand] = matchingQueue.headOption match {
    case Some(wPlayer) =>
      val candidates = candidatesFor(wPlayer)
      if (candidates.isEmpty)
        this.withUpdatedMatchingQueue(matchingQueue.tail).sendMatchNotFoundFor(wPlayer.key)
      else
        tryToMatch(wPlayer, candidates)

    case _ => this.ready
  }
}