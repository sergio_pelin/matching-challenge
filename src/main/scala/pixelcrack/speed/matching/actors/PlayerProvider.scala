package pixelcrack.speed.matching.actors

import java.lang.Math._

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout
import pixelcrack.speed.matching.actors.WaitingPool.{NewWaitingPlayer, WaitingPoolCommand}
import pixelcrack.speed.model.{Player, PlayerId}

import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}


/**
 * The component that knows how to operate on the `Player` data.
 * It doesn't have direct access to persistence, hence will talk to a `PlayerRepo`
 * implementation in order to retrieve, update and store `Player`s.
 * After retrieving the `Player` by id  and updating it, the `PlayerProvider` sends
 * `NewWaitingPlayer(player)` to the `WaitingPool`.
 */
object PlayerProvider {
  private val WIN: Int = 0
  private val LOSS: Int = 1

  sealed trait Message
  sealed trait PlayerProviderCommand extends Message
  final case class NewPlayer(playerId: PlayerId) extends PlayerProviderCommand
  final case class NewGameResult(winner: PlayerId, loser: PlayerId) extends PlayerProviderCommand

  private sealed trait PrivateCommand extends Message
  private final case class RequestSucceeded(player: Player) extends PrivateCommand
  private final case class RequestFailed(msg: String) extends PrivateCommand
  private final case class UpdateSucceeded(player: Player) extends PrivateCommand
  private final case class UpdateAll(players: Seq[Player]) extends PrivateCommand
  private final case class UpdateFailed(msg: String) extends PrivateCommand

  def apply(
    playerRepo: ActorRef[PlayerRepo.Query],
    waitingPool: ActorRef[WaitingPoolCommand]
  ):Behavior[PlayerProvider.Message] = {
    Behaviors.setup { _ => playerProvider(playerRepo, waitingPool) }
  }

  private def playerProvider(
    playerRepo: ActorRef[PlayerRepo.Query],
    waitingPool: ActorRef[WaitingPoolCommand]): Behavior[PlayerProvider.Message] = {

    Behaviors.setup[PlayerProvider.Message] { context =>
      implicit val timeout: Timeout = 1.second

      Behaviors.receiveMessage {

        case NewPlayer(id) =>
          context.log.info(s"*** New player arrived: $id")
          context.ask(playerRepo, ref => PlayerRepo.FindById(id, ref)) {
            case Success(PlayerRepo.FoundById(player)) => RequestSucceeded(player)
            case Success(PlayerRepo.Error(msg)) => RequestFailed(msg)
            case Failure(exception) => //TODO Retries and/or notification strategy should be applied here
              RequestFailed(exception.getMessage)
          }
          Behaviors.same

        case RequestSucceeded(player) =>
          context.log.info(s"*** Player successfully retrieved: $player")
          waitingPool ! NewWaitingPlayer(player)
          Behaviors.same

        case RequestFailed(error) =>
          //TODO Apply the chosen policy of error treatment.
          // Possibly send a warning notification to a security/administration module.
          context.log.error(s"*** Error retrieving player: $error")
          Behaviors.same

        case NewGameResult(winnerId, loserId) =>
          context.log.info(s"*** New game result arrived: ($winnerId, $loserId)")
          context.ask(playerRepo, ref => PlayerRepo.FindAllById(winnerId, loserId, ref)) {
            case Success(PlayerRepo.FoundAllById(winner, loser)) =>
              UpdateAll(Seq(updatedWinner(winner, loser), updatedLoser(loser, winner)))
            case Success(PlayerRepo.Error(msg)) => RequestFailed(msg)
            case Failure(exception) => //TODO Retries and/or notification strategy should be applied here
              RequestFailed(exception.getMessage)
          }
          Behaviors.same

        case UpdateAll(players) =>
          context.log.info(s"*** Players successfully updated: ${players.mkString(", ")}")
          players.foreach(p => updatePlayerRepo(context, playerRepo, p.id, p))
          Behaviors.same

        case UpdateSucceeded(player) =>
          context.log.info(s"*** Player successfully updated: $player")
          waitingPool ! NewWaitingPlayer(player)
          Behaviors.same

        case UpdateFailed(error) => //TODO Retries and/or notification strategy should be applied here
          context.log.error(s"*** Error updating player/s: $error")
          Behaviors.same
      }
    }
  }

  private def updatePlayerRepo(
    context: ActorContext[Message],
    playerRepository: ActorRef[PlayerRepo.Query],
    id: PlayerId,
    player: Player)(implicit timeout: Timeout): Unit = {
    context.ask(playerRepository, ref => PlayerRepo.Update(id, player, ref)){
      case Success(PlayerRepo.UpdateSuccess(player)) => UpdateSucceeded(player)
      case Success(PlayerRepo.Error(msg)) => UpdateFailed(msg)
      case Failure(exception) => UpdateFailed(exception.getMessage)
    }
  }

  private def updateScoreFor(gameOutcome: Int)(player: Player, opponent: Player): Player = {
    val updated = atan(tan(player.score) + pow(-1, gameOutcome) + max(0.01, abs(player.score - opponent.score)))
    player.copy(score = updated, playedOpponents = player.playedOpponents + opponent.id)
  }
  val updatedWinner: (Player, Player) => Player = updateScoreFor(WIN)
  val updatedLoser: (Player, Player) => Player = updateScoreFor(LOSS)

}
