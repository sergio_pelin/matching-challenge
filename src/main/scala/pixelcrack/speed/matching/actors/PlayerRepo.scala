package pixelcrack.speed.matching.actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import org.scalactic.{Bad, ErrorMessage, Good, Or}
import pixelcrack.speed.model.{Player, PlayerId}
import pixelcrack.speed.model.persistence.PlayerRepository


object PlayerRepo {

    sealed trait RepoCommand

    sealed trait Query extends RepoCommand
    final case class FindById(playerId: PlayerId, replyTo: ActorRef[Result]) extends Query
    final case class FindAllById(winnerId: PlayerId, loserId: PlayerId, replyTo: ActorRef[Result]) extends Query
    final case class Update(playerId: PlayerId, player: Player, replyTo: ActorRef[Result]) extends Query

    sealed trait Result extends RepoCommand
    final case class FoundById(player: Player) extends Result
    final case class FoundAllById(winner: Player, loser: Player) extends Result
    final case class UpdateSuccess(player: Player) extends Result
    final case class Error(msg: String) extends Result

}


/**
 * A layer of abstraction between the business logic and the persistence layer.
 */
trait PlayerRepo extends PlayerRepository {
    import PlayerRepo._

    protected def repo(): Behavior[Query] = {

        Behaviors.receiveMessage {

            case FindById(id, sender) =>
                findById(id) match {
                    case Good(player) => sender ! FoundById(player)
                    case Bad(error) => sender ! Error(s"Error retrieving player with $id: $error")
                }
                Behaviors.same

            case FindAllById(winner, loser, sender) =>
                (for {
                    w <- findById(winner)
                    l <- findById(loser)
                } yield (w, l)) match {
                    case Good((winnerId, loserId)) => sender ! FoundAllById(winnerId, loserId)
                    case Bad(error) => sender ! Error(s"Error retrieving players with [$winner] and [$loser]: $error")
                }
                Behaviors.same

            case Update(id, player, sender) =>
                update(id, player) match {
                    case Good(_) => sender ! UpdateSuccess(player)
                    case Bad(error) => sender ! Error(s"Error updating player $id: $error")
                }
                Behaviors.same
        }
    }
}

/**
 *  An implementation of `PlayerRepo` based on `Map`
 */
class MapPlayerRepo private(initialPlayers: Map[PlayerId, Player]) extends PlayerRepo {

    private var mapRepo: Map[PlayerId, Player] = initialPlayers

    override protected def findById(playerId: PlayerId): Or[Player, ErrorMessage] =
        mapRepo.get(playerId).map(Good(_)).getOrElse(Bad(s"Player with [$playerId] not found"))

    override protected def update(playerId: PlayerId, player: Player): Or[Unit, ErrorMessage] = {
        mapRepo = mapRepo.updated(playerId, player)
        Good(())
    }
}

object MapPlayerRepo {
    def apply(initialPlayers: Seq[Player]): Behavior[PlayerRepo.Query] =
        new MapPlayerRepo(initialPlayers.map(p => (p.id, p)).toMap).repo()
}


