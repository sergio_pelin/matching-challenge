package pixelcrack.speed.matching

import akka.actor.typed.ActorRef
import pixelcrack.speed.matching.actors.WaitingPlayerTimer.{PlayerTimerCommand, UpdateTimer}
import pixelcrack.speed.model.Player

/**
 * A wrapper over `Player` providing waiting functionalities.
 */
case class WaitingPlayer(
  player: Player,
  key: WaitKey,
  timer: ActorRef[PlayerTimerCommand],
  isOvertime: Boolean = false) extends Ordered[WaitingPlayer] {

  def withUpdatedTimer(newWaitMs: Long): WaitingPlayer = {
    timer ! UpdateTimer(newWaitMs)
    this
  }

  def markedOvertime: WaitingPlayer = copy(isOvertime = true)

  override def compare(that: WaitingPlayer): Int = {
    if (key < that.key) -1
    else if (key == that.key) 0
    else 1
  }
}
