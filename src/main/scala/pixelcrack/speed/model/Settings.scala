package pixelcrack.speed.model

case class Settings(maxScoreDelta: Double, maxWaitMs: Int)
