package pixelcrack.speed.model

case class Player(id: PlayerId, score: Double, playedOpponents: Set[PlayerId])
