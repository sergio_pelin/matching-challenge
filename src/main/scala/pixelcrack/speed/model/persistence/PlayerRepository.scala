package pixelcrack.speed.model.persistence

import org.scalactic.{ErrorMessage, Or}
import pixelcrack.speed.model.{Player, PlayerId}

trait PlayerRepository {
  protected def findById(playerId: PlayerId): Player Or ErrorMessage
  protected def update(playerId: PlayerId, player: Player): Unit Or ErrorMessage
}
