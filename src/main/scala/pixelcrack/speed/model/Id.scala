package pixelcrack.speed.model

trait Id[A]{
  def value: A
}

case class  PlayerId(value: Long) extends Id[Long]
