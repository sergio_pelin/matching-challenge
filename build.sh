#!/bin/bash
NAME=pixel-crack
REV=`git rev-parse --short HEAD`
tar -zcvf cr-$NAME-$REV.tgz -C project README.md project/build.properties src build.sbt
